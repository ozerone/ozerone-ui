# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.30](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.29...v0.0.30) (2020-11-14)

### [0.0.29](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.28...v0.0.29) (2020-09-03)


### Bug Fixes

* input changes ([f1f3074](https://gitlab.com/ozerone/ozerone-ui/commit/f1f3074d0cdd34506d918109c2db241fa98573f5))

### [0.0.28](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.27...v0.0.28) (2020-07-25)

### [0.0.27](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.26...v0.0.27) (2020-07-25)

### [0.0.26](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.25...v0.0.26) (2020-07-25)


### Features

* added inputButton component ([956b2a2](https://gitlab.com/ozerone/ozerone-ui/commit/956b2a20ce8850e8c872ccf79793650aa85b8fc1))

### [0.0.25](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.24...v0.0.25) (2020-05-31)

### [0.0.22](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.21...v0.0.22) (2020-05-23)


### Features

* added clickable option for text and headline ([d29a8e8](https://gitlab.com/ozerone/ozerone-ui/commit/d29a8e8ca154f85efaa9087c99f96399cc63a442))

### [0.0.19](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.18...v0.0.19) (2020-05-16)


### Features

* added textarea, disabled button ([6a50e4c](https://gitlab.com/ozerone/ozerone-ui/commit/6a50e4c75640debc0f423e942910abdf2d21a646))

### [0.0.18](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.17...v0.0.18) (2020-05-08)


### Features

* added event handlers to forms ([22a4e5a](https://gitlab.com/ozerone/ozerone-ui/commit/22a4e5aa0a7c37317114b9f4c4c326cfe7cd27c7))

### [0.0.17](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.16...v0.0.17) (2020-05-08)


### Features

* added checkbox, updated text and input ([2e81d89](https://gitlab.com/ozerone/ozerone-ui/commit/2e81d89ec246fe3781b160f702ae6a6cd84a525c))

### [0.0.16](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.15...v0.0.16) (2020-05-07)

### [0.0.15](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.14...v0.0.15) (2020-05-07)

### [0.0.14](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.13...v0.0.14) (2020-05-07)

### [0.0.13](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.12...v0.0.13) (2020-05-07)

### [0.0.12](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.11...v0.0.12) (2020-05-07)


### Features

* removed tokens and added new components to dist ([4d489f7](https://gitlab.com/ozerone/ozerone-ui/commit/4d489f7501a0a82d91ec319c8f372aac5249df75))

### [0.0.11](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.10...v0.0.11) (2020-05-07)


### Features

* added inputs, buttons and typography ([c9e4f8e](https://gitlab.com/ozerone/ozerone-ui/commit/c9e4f8ecefed4b1368b8bea628fc29dce1e70eeb))

### [0.0.10](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.9...v0.0.10) (2020-05-06)


### Features

* added text, headline and input components ([c0fcd0d](https://gitlab.com/ozerone/ozerone-ui/commit/c0fcd0d3a4bd42a5dffbc3eb127d514f1f506ae9))

### [0.0.9](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.8...v0.0.9) (2020-04-25)


### Bug Fixes

* changed component color ([9c5e7e1](https://gitlab.com/ozerone/ozerone-ui/commit/9c5e7e1cc633a2f93336a13c296eaf7d11c5c97f))

### [0.0.8](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.7...v0.0.8) (2020-04-25)


### Bug Fixes

* fixed import from ozerone ([6c9438d](https://gitlab.com/ozerone/ozerone-ui/commit/6c9438d622620c94680fd547f14da9056ed7087c))

### [0.0.7](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.2...v0.0.7) (2020-04-25)

### [0.0.2](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.6...v0.0.2) (2020-04-25)

### [0.0.6](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.5...v0.0.6) (2020-04-24)


### Bug Fixes

* fix gitlab commit version and push tag ([b3a738c](https://gitlab.com/ozerone/ozerone-ui/commit/b3a738c4f9eeb7c4d39105d38d1b153f1d9410cf))
* fix gitlab commit version and push tag ([1cb2ffb](https://gitlab.com/ozerone/ozerone-ui/commit/1cb2ffb39a4a1d5f3f805edaf533062c6389d343))

### [0.0.5](https://gitlab.com/ozerone/ozerone-ui/compare/v0.0.4...v0.0.5) (2020-04-24)
