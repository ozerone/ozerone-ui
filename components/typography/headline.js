import React from "react";
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { SizeFontHeadlineSmall, SizeFontHeadlineMedium, SizeFontHeadlineLarge, SizeFontHeadlineXlarge, FontWeightBold, ColorFontDefault, FontWeightSemibold } from "@ozerone/ozerone-ui-tokens";

export const Headline = props => {
  switch (props.size) {
    case 'S':
      return <StyledHeadlineSmall {...props}/>;
    case 'M':
      return <StyledHeadlineMedium {...props}/>;
    case 'L':
      return <StyledHeadlineLarge {...props}/>;
    case 'XL':
      return <StyledHeadlineXLarge {...props}/>;
    default:
      return <StyledHeadlineMedium {...props}/>;
  }
};

Headline.propTypes = {
  size: PropTypes.oneOf(['S', 'M', 'L', 'XL']),
  clickable: PropTypes.bool,
}

const headlineCommonStyles = ({clickable}) => ({
  fontWeight: FontWeightSemibold,
  color: ColorFontDefault,
  margin: 0,
  cursor: clickable ? "pointer" : "auto",
});

const StyledHeadlineXLarge = styled.h1(props => ({
  fontSize: SizeFontHeadlineXlarge,
  ...headlineCommonStyles(props),
  fontWeight: FontWeightBold
}));

const StyledHeadlineLarge = styled.h2(props => ({
  fontSize: SizeFontHeadlineLarge,
  ...headlineCommonStyles(props)
}));

const StyledHeadlineMedium = styled.h3(props => ({
  fontSize: SizeFontHeadlineMedium,
  ...headlineCommonStyles(props)
}));

const StyledHeadlineSmall = styled.h4(props => ({
  fontSize: SizeFontHeadlineSmall,
  ...headlineCommonStyles(props)
}));
