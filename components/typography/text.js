import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";
import {
  ColorBaseBlack,
  SizeFontTiny,
  SizeFontBase,
  SizeFontHeadlineMedium,
  SizeFontHeadlineLarge,
  SizeFontHeadlineXlarge,
  ColorFontPrimary,
  ColorFontSecondary,
  ColorFontLight,
  ColorFontDefault,
  FontWeightBold,
  FontWeightRegular,
  FontWeightSemibold,
} from "@ozerone/ozerone-ui-tokens";

export const Text = ({
  size,
  primary,
  secondary,
  light,
  customColor,
  bold,
  semibold,
  inline,
  ...rest
}) => {
  const styles = {};
  switch (size) {
    case "XS":
      styles.size = SizeFontTiny;
      break;
    case "S":
      styles.size = SizeFontBase;
      break;
    case "M":
      styles.size = SizeFontHeadlineMedium;
      break;
    case "L":
      styles.size = SizeFontHeadlineLarge;
      break;
    case "XL":
      styles.size = SizeFontHeadlineXlarge;
      break;
    default:
      styles.size = SizeFontBase;
  }

  styles.color = primary
    ? ColorFontPrimary
    : secondary
    ? ColorFontSecondary
    : light
    ? ColorFontLight
    : customColor || ColorFontDefault;

  styles.weight = bold
    ? FontWeightBold
    : semibold
    ? FontWeightSemibold
    : FontWeightRegular;

  return inline ? (
    <StyledSpan
      color={styles.color}
      size={styles.size}
      weight={styles.weight}
      {...rest}
    />
  ) : (
    <StyledParagraph
      color={styles.color}
      size={styles.size}
      weight={styles.weight}
      {...rest}
    />
  );
};

Text.propTypes = {
  size: PropTypes.oneOf(["XS", "S", "M", "L", "XL"]),
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  light: PropTypes.bool,
  customColor: PropTypes.string,
  bold: PropTypes.bool,
  semibold: PropTypes.bool,
  inline: PropTypes.bool,
  children: PropTypes.node,
  clickable: PropTypes.bool,
};

const textStyles = (props) => ({
  fontSize: props.size || SizeFontBase,
  fontWeight: props.weight || 400,
  color: props.color || ColorBaseBlack,
  cursor: props.clickable ? "pointer" : "auto",
});

const StyledParagraph = styled.p((props) => textStyles(props));

const StyledSpan = styled.span((props) => ({
  ...textStyles(props),
  display: "inline-block",
}));
