import React from "react";
import { Text } from "./text";

export default {
  title: "Typography/Text",
  component: Text,
};

const content =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ";

export const Default = () => <Text>{content}</Text>;
export const Semibold = () => <Text semibold>{content}</Text>;
export const Bold = () => <Text bold>{content}</Text>;
export const Colors = () => (
  <div>
    <p>
      <Text primary>{content}</Text>
      <Text primary semibold>
        {content}
      </Text>
    </p>
    <p>
      <Text secondary>{content}</Text>
      <Text secondary semibold>
        {content}
      </Text>
    </p>
    <p>
      <Text light>{content}</Text>
      <Text light semibold>
        {content}
      </Text>
    </p>
  </div>
);
export const Inline = () => (
  <div>
    <Text inline primary>
      {content}
    </Text>
    <Text inline secondary>
      {content}
    </Text>
  </div>
);
export const Sizes = () => (
  <div>
    <Text size="XS">Tiny font size</Text>
    <Text size="S">Base font size</Text>
    <Text size="M">Medium font size</Text>
    <Text size="L">Large font size</Text>
    <Text size="XL">Extra large font size</Text>
  </div>
);
export const Clickable = () => (
  <div>
    <Text primary semibold clickable onClick={() => alert("clicked")}>
      + New Project
    </Text>
    <Text secondary semibold clickable onClick={() => alert("clicked")}>
      + New Project
    </Text>
  </div>
);
