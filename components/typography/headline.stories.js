import React from 'react';
import { Headline } from './headline';

export default {
    title: "Typography/Headline",
    component: Headline,
};

const content = "Lorem ipsum dolor sit amet";

export const HeadlineExtraLarge = () => <Headline size="XL">{content}</Headline>;
export const HeadlineLarge = () => <Headline size="L">{content}</Headline>;
export const HeadlineMedium = () => <Headline size="M">{content}</Headline>;
export const HeadlineSmall = () => <Headline size="S">{content}</Headline>;
export const HeadlineClickable = () => <Headline clickable onClick={() => alert("clicked")}>{content}</Headline>;
