import React from "react";
import { Card } from "./card";
import { Headline } from "../typography/headline";
import { Text } from "../typography/text";

export default {
  title: "Elements/Card",
  Component: Card,
};

export const Default = () => (
  <Card>
    <Headline>Card header</Headline>
    <Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua.
    </Text>
  </Card>
);

export const PaddingSizes = () => (
  <div>
    <Card padding="no-padding">
      <Headline>Card header</Headline>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.
      </Text>
    </Card>
    <br />
    <Card padding="S">
      <Headline>Card header</Headline>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.
      </Text>
    </Card>
    <br />
    <Card padding="M">
      <Headline>Card header</Headline>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.
      </Text>
    </Card>
    <br />
    <Card padding="L">
      <Headline>Card header</Headline>
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.
      </Text>
    </Card>
  </div>
);

export const Bordered = () => (
  <div>
    <Card border="dashed" elevation="flat">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua.
    </Card>
    <br />
    <Card border="solid" elevation="flat">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua.
    </Card>
  </div>
);
