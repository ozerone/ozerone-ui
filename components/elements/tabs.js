import React from "react";
import { css } from "emotion";
import {
  BorderRadius2,
  ColorBaseGrayTint,
  ColorBaseWhite,
  ColorFontButtonSecondary,
  ColorFontPrimary,
  SizeFontBase,
} from "@ozerone/ozerone-ui-tokens";
import * as ReactTabs from "react-tabs";

const TabListClassName = css({
  margin: 0,
  padding: 0,
});

const TabClassName = css({
  display: "inline-block",
  height: 48,
  lineHeight: "48px",
  color: ColorFontButtonSecondary,
  borderColor: ColorBaseGrayTint,
  backgroundColor: ColorBaseWhite,
  padding: "0 20px",
  marginRight: "10px",
  borderWidth: "1px",
  borderRadius: BorderRadius2,
  fontSize: SizeFontBase,
  borderStyle: "solid",
  cursor: "pointer",
  "&.react-tabs__tab--selected": {
    borderColor: ColorFontPrimary,
    color: ColorFontPrimary,
  },
  "&.react-tabs__tab--disabled": {
    opacity: ".6",
    cursor: "not-allowed",
  },
});

export class Tabs extends ReactTabs.Tabs {}

export class TabList extends ReactTabs.TabList {
  constructor(props) {
    super(props);
  }

  render() {
    const props = {
      ...this.props,
      className: `${TabListClassName} ${this.props.className}`,
    };
    return <ReactTabs.TabList {...props} />;
  }
}

export class Tab extends ReactTabs.Tab {
  constructor(props) {
    super(props);
  }

  render() {
    const props = {
      ...this.props,
      className: `${TabClassName} ${this.props.className}`,
    };
    return <ReactTabs.Tab {...props} />;
  }
}

export class TabPanel extends ReactTabs.TabPanel {}
