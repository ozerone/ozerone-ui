import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card } from "./card";
import styled from "@emotion/styled";

export const ExpandableCard = ({ children, open, trigger, triggerClick, title, titleClick }) => {
  return (
    <Card padding="no-padding">
      <StyledHeader>
        <StyledTrigger onClick={triggerClick}>
          {trigger}
        </StyledTrigger>
        <StyledTitle onClick={titleClick}>
          {title}
        </StyledTitle>
      </StyledHeader>
      {
        open && children
      }
    </Card>
  );
};

ExpandableCard.propTypes = {
  trigger: PropTypes.element,
  title: PropTypes.element,
  children: PropTypes.children,
  open: PropTypes.bool,
  triggerClick: PropTypes.func,
  titleClick: PropTypes.func,
};

const StyledHeader = styled.div((props) => ({
  height: "78px",
  display: "flex",
}));

const StyledTrigger = styled.div(() => ({
  flexBasis: "78px",
  flexGrow: 0,
  flexShrink: 0,
}));

const StyledTitle = styled.div(() => ({
  flexGrow: 1,
}));