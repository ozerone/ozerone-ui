import React from "react";
import { ExpandableCard } from "../elements/expandable-card";
import { Headline } from "../typography/headline";
import { Text } from "../typography/text";

export default {
  title: "Elements/ExpandableCard",
  Component: ExpandableCard,
};

export const Default = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  return (
    <ExpandableCard
      open={isOpen}
      title={<Text>Card title</Text>}
      titleClick={() => console.log("title clicked")}
      trigger={
        <div style={{ width: "100%", height: "100%", backgroundColor: "blue" }}>
          Test
        </div>
      }
      triggerClick={() => setIsOpen(!isOpen)}
    >
      Content goes here
    </ExpandableCard>
  );
};
