import React from "react";
import { Tabs, Tab, TabList, TabPanel } from "./tabs";

export default {
  title: "Elements/Tabs",
  Component: Tabs,
};

export const Default = () => {
  return (
    <Tabs>
      <TabList>
        <Tab>First tab</Tab>
        <Tab disabled>Second tab</Tab>
        <Tab>Third tab</Tab>
      </TabList>
      <TabPanel>First content</TabPanel>
      <TabPanel>Second content</TabPanel>
      <TabPanel>Third content</TabPanel>
    </Tabs>
  );
};
