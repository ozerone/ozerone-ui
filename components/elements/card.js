import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";
import {
  BorderRadius2,
  ColorBaseWhite,
  ColorBaseSilverPure,
  Spacing1,
  Spacing2,
  Spacing3,
} from "@ozerone/ozerone-ui-tokens";

export const Card = (props) => {
  return <StyledCard {...props} />;
};

Card.propTypes = {
  padding: PropTypes.oneOf(["no-padding", "S", "M", "L"]),
  border: PropTypes.oneOf(["none", "dashed", "solid"]),
  elevation: PropTypes.oneOf(["flat", "raised"]),
};

const StyledCard = styled.div(
  ({ padding, border = "none", elevation = "raised" }) => {
    let paddingValue = Spacing2;

    padding === "no-padding" && (paddingValue = 0);
    padding === "S" && (paddingValue = Spacing1);
    padding === "M" && (paddingValue = Spacing2);
    padding === "L" && (paddingValue = Spacing3);

    const dashedBorder =
      `data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='8' ry='8' stroke='%23ACADBFFF' stroke-width='2' stroke-dasharray='6%2c 14' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e`;

    const borderStyles = {};
    if (border === "dashed") {
      borderStyles.backgroundImage = `url("${dashedBorder}")`;
    } else if (border === "solid") {
      borderStyles.border = `1px solid ${ColorBaseSilverPure}`;
    }

    return {
      padding: paddingValue,
      borderRadius: BorderRadius2,
      boxShadow: elevation === "flat" ? "none" : "0 4px 8px 0 rgba(0,0,0,0.08);",
      backgroundColor: ColorBaseWhite,
      overflow: "hidden",
      ...borderStyles
    };
  }
);
