import React from 'react'
import styled from '@emotion/styled';
import { Text } from './../typography/text';
import { ColorBorderInputDefault } from '@ozerone/ozerone-ui-tokens';

export const Checkbox = props => {
  return (
    <label className={props.className}>
      <StyledInput 
        type="checkbox" 
        name={props.name} 
        disabled={props.disabled}
      />
      <Text inline semibold>{props.label}</Text>
    </label>
  );
}

const StyledInput = styled.input(() => ({
  display: 'inline-block',
  width: '16px',
  height: '16px',
  margin: '0 8px 0 0',
  borderWidth: '1px',
  borderStyle: 'solid',
  borderColor: ColorBorderInputDefault
}));