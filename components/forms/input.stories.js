import React from "react";
import { Input } from "./input";

export default {
  title: "Forms/Input",
  Component: Input
};

export const Default = () => (
  <div>
    <Input placeholder="Default input" label="Simple text input" onChange={(e) => console.log(e)}/>
    <Input placeholder="Full width input" fullWidth={true} label="Simple text input" />
  </div>
);
export const Error = () => <Input error placeholder="Something" label="Invalid input" />;
export const Disabled = () => <Input disabled placeholder="Something" label="Disabled input" />;
