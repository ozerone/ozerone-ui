import React from "react";
import { Textarea } from "./textarea";

export default {
  title: "Forms/Textarea",
  Component: Textarea
};

export const Default = () => (
  <div>
    <Textarea rows={6} placeholder="Default input" label="Simple text input" onChange={(e) => console.log(e)}/>
    <Textarea placeholder="Full width input" fullWidth={true} label="Simple text input" />
  </div>
);
export const Error = () => <Textarea error placeholder="Something" label="Invalid input" />;
export const Disabled = () => <Textarea disabled placeholder="Something" label="Disabled input" />;
export const WithValue = () => <Textarea value="Dummy data" placeholder="Something" label="Prefilled data" />;
