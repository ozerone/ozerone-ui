import React from "react";
import { InputButton } from "./input-button";

export default {
  title: "Forms/InputButton",
  Component: InputButton,
};

export const Default = () => (
  <div>
    <InputButton
      placeholder="Default input"
      label="Simple text input"
      onChange={(e) => console.log(e)}
      btnValue="Show"
      onClick={() => console.log('click')}
    />
  </div>
);
// export const Error = () => <Input error placeholder="Something" label="Invalid input" />;
export const Disabled = () => (
  <InputButton
    disabled
    placeholder="Something"
    label="Disabled input"
    btnValue="Show"
  />
);
