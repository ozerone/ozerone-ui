import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";
import { Text } from "../typography/text";
import {
  BorderRadius2,
  Spacing5,
  ColorBorderInputDefault,
  ColorBorderInputActive,
  ColorBorderInputError,
  ColorFontInputActive,
  SizeFontBase,
  Spacing1,
  Spacing2,
  ColorFontInputPlaceholder,
  ColorBackgroundInputDisabled,
} from "@ozerone/ozerone-ui-tokens";

export const Input = (props) => {
  const {
    error,
    disabled,
    fullWidth,
    label,
    name,
    type,
    placeholder,
    onChange,
    onBlur,
    value,
    ...rest
  } = props;
  let className = error ? " error" : "";
  className += disabled ? " disabled" : "";

  return (
    <InputStyled fullWidth={fullWidth}>
      {label && (
        <Text semibold inline>
          {label}
        </Text>
      )}
      <InputWrapperStyle className={className}>
        <input
          name={name}
          type={type}
          placeholder={placeholder}
          disabled={disabled}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          {...rest}
        />
      </InputWrapperStyle>
    </InputStyled>
  );
};

Input.propTypes = {
  fullWidth: PropTypes.bool,
  error: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
};

const InputStyled = styled.label((props) => ({
  display: "inline-block",
  width: props.fullWidth ? "100%" : "auto",
  "& > span": {
    marginBottom: Spacing1,
  },
}));

const InputWrapperStyle = styled.div(() => ({
  borderRadius: BorderRadius2,
  borderWidth: "1px",
  borderStyle: "solid",
  borderColor: ColorBorderInputDefault,
  height: Spacing5,
  boxSizing: "border-box",
  overflow: "hidden",
  "& input": {
    appearance: "none",
    border: "none",
    height: "100%",
    width: "100%",
    boxSizing: "border-box",
    outline: "none",
    backgroundColor: "transparent",
    fontSize: SizeFontBase,
    color: ColorFontInputActive,
    paddingLeft: Spacing2,
    paddingRight: Spacing2,
    "&::placeholder": {
      color: ColorFontInputPlaceholder,
    },
  },
  "&:focus-within": {
    borderColor: ColorBorderInputActive,
  },
  "&.error": {
    borderColor: ColorBorderInputError,
  },
  "&.disabled": {
    backgroundColor: ColorBackgroundInputDisabled, //TODO: should be used ColorBackgroundInputDisabled after UI tokens update
    border: "none",
  },
}));
