import React from "react";
import { UploadButton } from "./upload-button";

export default {
  title: "Forms/UploadButton",
  Component: UploadButton,
};

export const Default = () => (
  <div>
    <UploadButton>Click me</UploadButton>
  </div>
);
