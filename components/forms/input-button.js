import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";
import { Text } from "../typography/text";
import {
  BorderRadius2,
  Spacing5,
  ColorBorderInputDefault,
  ColorBorderInputActive,
  ColorBorderInputError,
  ColorFontInputActive,
  SizeFontBase,
  Spacing1,
  Spacing2,
  ColorFontInputPlaceholder,
  ColorBackgroundInputDisabled,
  ColorBackgroundButtonPrimary,
  ColorFontButtonPrimary,
  ColorFontInputDisabled,
} from "@ozerone/ozerone-ui-tokens";

export const InputButton = (props) => {
  let className = props.error ? " error" : "";
  className += props.disabled ? " disabled" : "";

  return (
    <InputStyled fullWidth={props.fullWidth}>
      {props.label && (
        <Text semibold inline>
          {props.label}
        </Text>
      )}
      <InputWrapperStyle className={className}>
        <input
          name={props.name}
          type={props.type}
          placeholder={props.placeholder}
          onChange={props.onChange}
          onBlur={props.onBlur}
          value={props.value}
        />
        <button disabled={props.disabled} onClick={props.onClick}>
          {props.btnValue}
        </button>
      </InputWrapperStyle>
    </InputStyled>
  );
};

InputButton.propTypes = {
  fullWidth: PropTypes.bool,
  error: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onClick: PropTypes.func,
  value: PropTypes.string,
  btnValue: PropTypes.string,
};

const InputStyled = styled.label((props) => ({
  display: "inline-block",
  width: props.fullWidth ? "100%" : "auto",
  "& > span": {
    marginBottom: Spacing1,
  },
}));

const InputWrapperStyle = styled.div(() => ({
  borderRadius: BorderRadius2,
  borderWidth: "1px",
  borderStyle: "solid",
  borderColor: ColorBorderInputDefault,
  height: Spacing5,
  boxSizing: "border-box",
  overflow: "hidden",
  display: "flex",
  "& input": {
    appearance: "none",
    border: "none",
    height: "100%",
    width: 120,
    boxSizing: "border-box",
    outline: "none",
    backgroundColor: "transparent",
    fontSize: SizeFontBase,
    color: ColorFontInputActive,
    paddingLeft: Spacing2,
    paddingRight: Spacing2,
    textAlign: "center",

    "&::placeholder": {
      color: ColorFontInputPlaceholder,
    },
  },
  "& button": {
    appearance: "none",
    outline: "none",
    boxSizing: "border-box",
    padding: "0 20px",
    borderWidth: "1px",
    fontSize: SizeFontBase,
    cursor: "pointer",
    border: "0",
    backgroundColor: ColorBackgroundButtonPrimary,
    color: ColorFontButtonPrimary,

    "&, &:active": {
      borderStyle: "solid",
    },
  },
  "&:focus-within": {
    borderColor: ColorBorderInputActive,
  },
  "&.error": {
    borderColor: ColorBorderInputError,
  },
  "&.disabled": {
    button: {
      backgroundColor: ColorBackgroundInputDisabled,
      color: ColorFontInputDisabled,
    },
  },
}));
