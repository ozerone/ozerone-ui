import React from 'react'
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { Text } from '../typography/text';
import { 
  BorderRadius2, 
  Spacing5, 
  ColorBorderInputDefault,
  ColorBorderInputActive,
  ColorBorderInputError,
  ColorFontInputActive,
  SizeFontBase,
  Spacing1,
  Spacing2,
  ColorFontInputPlaceholder,
  ColorBackgroundInputDisabled
} from '@ozerone/ozerone-ui-tokens';

export const Textarea = props => {
  let className = props.error ? ' error' : '';
  className += props.disabled ? ' disabled' : '';
  
  return (
    <TextareaStyled fullWidth={props.fullWidth}>
      {
        props.label && (
          <Text semibold inline>{props.label}</Text>
        )
      }
      <TextareaWrapperStyle className={className}>
        <textarea 
          name={props.name} 
          type={props.type} 
          placeholder={props.placeholder} 
          disabled={props.disabled}
          onChange={props.onChange}
          onBlur={props.onBlur}
          value={props.value}
          rows={props.rows || 5}
        />
      </TextareaWrapperStyle>
    </TextareaStyled>
  );
}

Textarea.propTypes = {
  fullWidth: PropTypes.bool,
  error: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlue: PropTypes.func,
  value: PropTypes.string,
  rows: PropTypes.number
};

const TextareaStyled = styled.label(props => ({
  display: 'inline-block',
  width: props.fullWidth ? '100%' : 'auto',
  '& > span': {
    marginBottom: Spacing1
  }
}));

const TextareaWrapperStyle = styled.div(() => ({
  borderRadius: BorderRadius2,
  borderWidth: '1px',
  borderStyle: 'solid',
  borderColor: ColorBorderInputDefault,
  minHeight: Spacing5,
  boxSizing: 'border-box',
  overflow: 'hidden',
  '& textarea': {
    appearance: 'none',
    border: 'none',
    height: '100%',
    width: '100%',
    boxSizing: 'border-box',
    outline: 'none',
    backgroundColor: 'transparent',
    fontSize: SizeFontBase,
    color: ColorFontInputActive,
    padding: Spacing2,
    '&::placeholder': {
      color: ColorFontInputPlaceholder
    }
  },
  '&:focus-within': {
    borderColor: ColorBorderInputActive
  },
  '&.error': {
    borderColor: ColorBorderInputError
  },
  '&.disabled': {
    backgroundColor: ColorBackgroundInputDisabled, //TODO: should be used ColorBackgroundInputDisabled after UI tokens update
    border: 'none'
  }
}));