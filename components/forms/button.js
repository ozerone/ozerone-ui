import React from 'react'
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { 
  ColorBackgroundButtonPrimary, 
  ColorFontButtonPrimary, 
  ColorBackgroundButtonSecondary, 
  ColorFontButtonSecondary, 
  ColorBackgroundButtonOutline, 
  ColorFontButtonOutline,
  BorderRadius2,
  SizeFontBase,
  ColorBorderButtonOutline,
  ColorBorderButtonSecondary,
  ColorBackgroundButtonDisabled,
  ColorBaseGrayTint
} from '@ozerone/ozerone-ui-tokens';

export const Button = props => (
  <StyledButton {...props} />
)

Button.propTypes = {
  fullWidth: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  disabled: PropTypes.bool,
  outline: PropTypes.bool,
  size: PropTypes.oneOf(['S', 'M', 'L']),
  onClick: PropTypes.func,
  onSubmit: PropTypes.func
}

const primaryStyles = {
  backgroundColor: ColorBackgroundButtonPrimary,
  color: ColorFontButtonPrimary,
  borderWidth: '0'
};

const secondaryStyles = {
  backgroundColor: ColorBackgroundButtonSecondary,
  color: ColorFontButtonSecondary,
  borderColor: ColorBorderButtonSecondary
};

const outlineStyles = {
  backgroundColor: ColorBackgroundButtonOutline,
  color: ColorFontButtonOutline,
  borderColor: ColorBorderButtonOutline
};

const disabledStyles = {
  backgroundColor: ColorBackgroundButtonDisabled,
  color: ColorBaseGrayTint,
  borderWidth: 0
}

const defaultStyles = {
  backgroundColor: ColorBackgroundButtonSecondary,
  color: ColorFontButtonSecondary,  
};

const StyledButton = styled.button(({fullWidth, primary, secondary, outline, disabled, size}) => { 
  let additionalStyles = primary 
    ? primaryStyles 
    : secondary 
    ? secondaryStyles
    : outline
    ? outlineStyles 
    : disabled
    ? disabledStyles 
    : defaultStyles;
  
  const height = size === 'L' 
    ? '48px' 
    : size === 'M'
    ? '40px'
    : size === 'S'
    ? '30px' 
    : '48px';

  return {
    height,
    appearance: 'none',
    outline: 'none',
    boxSizing: 'border-box',
    minWidth: '170px',
    width: fullWidth ? '100%' : 'auto',
    padding: '0 20px',
    borderWidth: '1px',
    borderRadius: BorderRadius2, 
    fontSize: SizeFontBase,
    cursor: 'pointer',
    ...additionalStyles,
    
    '&, &:active': {
      borderStyle: 'solid',
    }
  };
});