import React from "react";
import { Button } from "./button";

export default {
  title: "Forms/Button",
  Component: Button
};

export const Default = () => <div>
  <Button size="S">Click me</Button>
  <Button size="M">Click me</Button>
  <Button size="L">Click me</Button>
</div>;
export const Primary = () => <Button primary>Primary</Button>;
export const Clickable = () => <Button primary onClick={() => alert('clicked')}>Primary</Button>;
export const Secondary = () => <Button secondary>Secondary</Button>;
export const Outline = () => <Button outline>Outline</Button>;
export const Disabled = () => <Button disabled>Disabled</Button>;
export const LongText = () => <Button primary>Primary button with long text</Button>;
export const FullWidth = () => <Button primary fullWidth>Primary button with long text</Button>;
