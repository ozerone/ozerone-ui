import React from "react";
import { Checkbox } from "./checkbox";

export default {
  title: "Forms/Checkbox",
  component: Checkbox,
};

export const Default = () => <Checkbox label="Remember me" />