import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";
import {
  ColorBaseSilverPure,
  ColorFontButtonSecondary,
  BorderRadius2,
  SizeFontBase,
  ColorBaseGrayTint,
} from "@ozerone/ozerone-ui-tokens";

export const UploadButton = (props) => <StyledUploadButton {...props} />;

UploadButton.propTypes = {
  onClick: PropTypes.func,
  onSubmit: PropTypes.func,
};

const StyledUploadButton = styled.button(() => ({
  height: "32px",
  appearance: "none",
  outline: "none",
  boxSizing: "border-box",
  minWidth: "140px",
  padding: "0 20px",
  borderWidth: "1px",
  borderRadius: BorderRadius2,
  fontSize: SizeFontBase,
  cursor: "pointer",
  backgroundColor: ColorBaseSilverPure,
  border: `1px solid ${ColorBaseGrayTint}`,
  color: ColorFontButtonSecondary,
}));
